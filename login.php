<?php  require_once('include/header.php'); ?>
	
	<section id="form"><!--form-->
		<div class="container">
			<div class="row">
				<div class="col-sm-4 col-sm-offset-1">
					<div class="login-form"><!--login form-->
						<h2>Login to your account</h2>
						<form action="process/process.php?action=login"  method="post">
							<input type="text" placeholder="Name" name="name" />
							<div></div>
							<input type="password" placeholder="Password" name="password" />
							<span>
								
							</span>
							<button type="submit" class="btn btn-default" name="login">Login</button>
						</form>
					</div><!--/login form-->
				</div>
				<div class="col-sm-1">
					<h2 class="or">OR</h2>
				</div>
				<div class="col-sm-4">
					<div class="signup-form"><!--sign up form-->
						<h2>New User Signup!</h2>
						<form action="process/process.php?action=registration" method="post">
							<input type="text" placeholder="Name" name="name" id="name1" />
							<input type="email" placeholder="Email Address" name="email" />
							<input type="password" placeholder="Password" name="password" />
							<button type="submit" class="btn btn-default" name="registration">Sigup</button>
						</form>
						
					</div><!--/sign up form-->
				</div>
			</div>
		</div>
	</section><!--/form-->
	<?php
		require_once ('include/footer.php');
	?>

</body>
</html>