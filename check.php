<?php include 'include/header.php'; ?>
<!--/header-->
<?php if(isset($_SESSION['cart'])) {?>
	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="index.php">Home</a></li>
				  <li class="active">Check out</li>
				</ol>
			</div>

			<div class="review-payment">
				<h2>Product Information(You have <?php echo count($_SESSION['cart']); ?> product)</h2>
			</div>
		<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description"></td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
							<td></td>
						</tr>
					</thead>
					<?php 
				$_SESSION['total_money'] = 0;
				$_SESSION['total_product']=0;

				foreach ($_SESSION['cart'] as $key ) {
					$_SESSION['total_money'] += $key['total_money_cart'];
					$_SESSION['total_product'] +=$key['cart'];
						?>
	
					<tbody>
						<tr>
							
							
							<td class="cart_product">
								<a href=""><img src="acsses/images/cart/three.png" alt=""></a>
							</td>
							<td class="cart_description">
								<h4><a href=""><?php echo $key['description_product']; ?></a></h4>
							
							</td>
							<td class="cart_price">
								<p>$<?php echo $key['price_product']; ?></p>
							</td>
							<td class="cart_quantity">
								<!-- <div class="cart_quantity_button"> -->								
									<input class="cart_quantity_input" type="text" name="quantity" value="<?php echo $key['count(var)'] ;?>" " size="2">
									
							<!-- 	</div> -->
							</td>
							<td class="cart_total">
								<p class="cart_total_price">$<?php echo $key['total_money_cart'] ;?></p>
							</td>
							<td class="cart_delete">
								
									<form action="process/process.php?action=remove_product" method="post">
									<input type="hidden" value=<?php echo $cart['id_muaban']?> name="id_cart"/>
									<!-- <button class="remove-cart" name="remove"> Xóa Sản Phẩm </button> -->
									<a class="cart_quantity_delete" name="remove" ><i class="fa fa-times"></i>
										</a>
									</form>
							</td>
						</tr>
						<tr>
							
						</tr>
						<tr>
							<td colspan="4">&nbsp;</td>
							<td colspan="2">
								<table class="table table-condensed total-result">
									<tr>
										<td>Cart Sub Total</td>
										<td>$<?php echo $_SESSION['total_product']; ?></td>
									</tr>
									
									<tr class="shipping-cost">
										<td>Shipping Cost</td>
										<td>Free</td>										
									</tr>
									<tr>
										<td>Total</td>
										<td><span>$<?php echo $_SESSION["total_money"]; ?></span></td>
									</tr>
									<tr>
										<form method="post" action="process/process.php?action=buy">
											
											<td><a class="btn btn-primary" name="buy" type="submit">Buy</a></td>
										<td><span><a href="product.php" name="tiep_tuc" class="btn btn-primary" type="submit">
Continue to buy products</a></span></td>
										</form>
									</tr>
		
								</table>
							</td>
						</tr>
					</tbody>
					<?php }?>
				</table>
			</div>
			
		</div>
	</section>

<?php } ?>

		
		<?php include 'include/footer.php'; ?>