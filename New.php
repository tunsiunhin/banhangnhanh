<?php require_once 'db/database.php' ?>
<?php require_once 'include/header.php'; ?>
	
	<section id="advertisement">
		<div class="container">
			<img src="acsses/images/shop/advertisement.jpg" alt="" />
		</div>
	</section>
	
	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="left-sidebar">
						<h2>Category</h2>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordian" href="#sportswear">
											<span class="badge pull-right"><i class="fa fa-plus"></i></span>
											Sportswear
										</a>
									</h4>
								</div>
								<div id="sportswear" class="panel-collapse collapse">
									<div class="panel-body">
										<ul>
											<li><a href="">Nike </a></li>
											<li><a href="">Under Armour </a></li>
											<li><a href="">Adidas </a></li>
											<li><a href="">Puma</a></li>
											<li><a href="">ASICS </a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordian" href="#mens">
											<span class="badge pull-right"><i class="fa fa-plus"></i></span>
											Mens
										</a>
									</h4>
								</div>
								<div id="mens" class="panel-collapse collapse">
									<div class="panel-body">
										<ul>
											<li><a href="">Fendi</a></li>
											<li><a href="">Guess</a></li>
											<li><a href="">Valentino</a></li>
											<li><a href="">Dior</a></li>
											<li><a href="">Versace</a></li>
											<li><a href="">Armani</a></li>
											<li><a href="">Prada</a></li>
											<li><a href="">Dolce and Gabbana</a></li>
											<li><a href="">Chanel</a></li>
											<li><a href="">Gucci</a></li>
										</ul>
									</div>
								</div>
							</div>
							
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordian" href="#womens">
											<span class="badge pull-right"><i class="fa fa-plus"></i></span>
											Womens
										</a>
									</h4>
								</div>
								<div id="womens" class="panel-collapse collapse">
									<div class="panel-body">
										<ul>
											<li><a href="">Fendi</a></li>
											<li><a href="">Guess</a></li>
											<li><a href="">Valentino</a></li>
											<li><a href="">Dior</a></li>
											<li><a href="">Versace</a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="#">Kids</a></h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="#">Fashion</a></h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="#">Households</a></h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="#">Interiors</a></h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="#">Clothing</a></h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="#">Bags</a></h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="#">Shoes</a></h4>
								</div>
							</div>
						</div><!--/category-productsr-->
					
						<div class="brands_products"><!--brands_products-->
							<h2>Brands</h2>
							<div class="brands-name">
								<ul class="nav nav-pills nav-stacked">
									<li><a href=""> <span class="pull-right">(50)</span>Acne</a></li>
									<li><a href=""> <span class="pull-right">(56)</span>Grüne Erde</a></li>
									<li><a href=""> <span class="pull-right">(27)</span>Albiro</a></li>
									<li><a href=""> <span class="pull-right">(32)</span>Ronhill</a></li>
									<li><a href=""> <span class="pull-right">(5)</span>Oddmolly</a></li>
									<li><a href=""> <span class="pull-right">(9)</span>Boudestijn</a></li>
									<li><a href=""> <span class="pull-right">(4)</span>Rösch creative culture</a></li>
								</ul>
							</div>
						</div><!--/brands_products-->
						
						<div class="price-range"><!--price-range-->
							<h2>Price Range</h2>
							<div class="well">
								 <input type="text" class="span2" value="" data-slider-min="0" data-slider-max="600" data-slider-step="5" data-slider-value="[250,450]" id="sl2" ><br />
								 <b>$ 0</b> <b class="pull-right">$ 600</b>
							</div>
						</div><!--/price-range-->
						
						<div class="shipping text-center"><!--shipping-->
							<img src="acsses/images/home/shipping.jpg" alt="" />
						</div><!--/shipping-->
						
					</div>
				</div>
				
				<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Features Items</h2>
						<?php 
						$sql = "SELECT id_product FROM product WHERE id_gr_product =3 ORDER BY date_gr DESC LIMIT 12";
						$query = mysqli_query($con, $sql);
						$tong_so_cot = mysqli_num_rows($query);
						$trang_hien_tai= isset($_GET['page']) ? $_GET['page'] : 1;
						$so_tin_trang = 12;
						$tong_so_trang = ceil($tong_so_cot / $so_tin_trang);
						if ($trang_hien_tai > $tong_so_trang) {
							$trang_hien_tai = $tong_so_trang;
						}elseif ($trang_hien_tai < 1) {
							$trang_hien_tai = 1;
						}
						$hi = ($trang_hien_tai -1) *$so_tin_trang;
						$result = "SELECT * FROM product LIMIT $hi,$so_tin_trang";

						?>
						<?php 
						while ($row1 = mysql_fetch_assoc($result)) {
						echo	'<div class="col-sm-4">';
						echo	'<div class="product-image-wrapper">';
						echo	'<div class="single-products">';
						echo	'<div class="productinfo text-center">';
						echo	'<img src="'.$row1['img_product'].'" alt="" />';
						echo				'<h2>$'.$row1['price_name'].'</h2>';
						echo				'<p>'.$row1['name_product'].'</p>';
						echo		'<a href="#" class="btn btn-default add-to-cart">';
						echo		'<i class="fa fa-shopping-cart">';
						echo		 '</i>Add to cart</a>';
						echo			'</div>';
						echo			'<div class="product-overlay">';
						echo				'<div class="overlay-content">';
						echo					'<h2>$'.$row1['price_name'].'</h2>';
						echo					'<p>'.$row1['description_product'].'</p>';
						echo					'<a href="#" class="btn btn-default add-to-cart">';
						echo					'<i class="fa fa-shopping-cart">';
						echo					'</i>Add to cart</a>';
						echo				'</div>';
						echo			'</div>';
						echo		'</div>';
						echo		'<div class="choose">';
						echo			'<ul class="nav nav-pills nav-justified">';
						echo				'<li>';
						echo				'<a href=""><i class="fa fa-plus-square">';
						echo					'</i>Add to wishlist</a>';
						echo							'</li>';
						echo						'<li>';
						echo					'<a href="">';
						echo				'<i class="fa fa-plus-square">';
						echo				'</i>Add to compare</a>';
						echo				'</li>';
						echo			'</ul>';
						echo		'</div>';
						echo	'</div>';
						echo '</div>';
						}
						?>
						<?php 
						if ($trang_hien_tai >1 & $tong_so_trang >1) {
							echo '<ul class="pagination">';
							echo '<li><a href="shop.php?page='.($trang_hien_tai -1).'"> Prev</a></li>';

							for ($i=1; $i <= $tong_so_trang ; $i++) { 
								if ($i == $trang_hien_tai) {
									echo '<span>'.$i.'</span>';
								}
								else{
									echo '<li><a href="shop.php?page='.$i.'">'.$i.'</a></li>';
								}
							}
							if ($trang_hien_tai < $trang_hien_tai & $trang_hien_tai > 1) {
								echo '<li><a href="shop.php?page='.($trang_hien_tai +1).'">Next</a></li>';
							}
							echo '</ul>';
						}

						?>
						<!-- <ul class="pagination">
							<li class="active"><a href="">1</a></li>
							<li><a href="">2</a></li>
							<li><a href="">3</a></li>
							<li><a href="">&raquo;</a></li>
						</ul> -->
					</div><!--features_items-->
				</div>
			</div>
		</div>
	</section>
	
<!--/Footer-->
<?php include 'include/footer.php'; ?>

  
    <script src="js/jquery.js"></script>
	<script src="js/price-range.js"></script>
    <script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/main.js"></script>
</body>
</html>
