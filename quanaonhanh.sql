-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: localhost:3306
-- Thời gian đã tạo: Th8 24, 2018 lúc 01:09 PM
-- Phiên bản máy phục vụ: 5.7.19
-- Phiên bản PHP: 5.6.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `quanaonhanh`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `contact`
--

CREATE TABLE `contact` (
  `id_contact` int(50) NOT NULL,
  `name_contact` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_contact` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_contact` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mesage_contact` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customers`
--

CREATE TABLE `customers` (
  `id` int(20) NOT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `join_time` int(20) NOT NULL,
  `adresss` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `customers`
--

INSERT INTO `customers` (`id`, `name`, `email`, `password`, `join_time`, `adresss`) VALUES
(1, 'dods', 'dasda', 'asdasd', 1535113739, ''),
(2, 'dogs', 'teu@mgail.com', 'c4ca4238a0b923820dcc509a6f75849b', 1535114221, ''),
(3, 'Tuan Luu', 'lqtuan.hd@gmail.com', '65eebb932a1d324033b3cf3fd8ff036f', 1535114329, ''),
(4, 'Tuan Luu', 'thienglineg@gmail.com', 'c81e728d9d4c2f636f067f89cc14862c', 1535114461, ''),
(5, 'tuan Luu', 'test@gmail.com', 'b7f1c3c1fb5279124145cefcb55dda53', 1535114483, '');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `cus_order`
--

CREATE TABLE `cus_order` (
  `id` int(50) DEFAULT NULL,
  `id_order_cus` int(10) NOT NULL,
  `total_money_order` int(50) DEFAULT NULL,
  `date_order_cus` datetime DEFAULT NULL,
  `status_order_cus` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `names__order_cus` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_order_cus` int(11) DEFAULT NULL,
  `addrres_order_cus` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_order_cus` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `cus_order`
--

INSERT INTO `cus_order` (`id`, `id_order_cus`, `total_money_order`, `date_order_cus`, `status_order_cus`, `names__order_cus`, `phone_order_cus`, `addrres_order_cus`, `email_order_cus`) VALUES
(31, 1, 3, '2018-08-17 06:00:00', 'mua nhanh cái nào', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `dentails_order`
--

CREATE TABLE `dentails_order` (
  `id_order_details` int(10) NOT NULL,
  `id_order_cus` int(10) DEFAULT NULL,
  `id_product` int(10) DEFAULT NULL,
  `quality_order_dentails` int(20) DEFAULT NULL,
  `money_product` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `dentails_order`
--

INSERT INTO `dentails_order` (`id_order_details`, `id_order_cus`, `id_product`, `quality_order_dentails`, `money_product`) VALUES
(2, 1, 1, 3, 3);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `gr_product`
--

CREATE TABLE `gr_product` (
  `id_gr_product` int(10) NOT NULL,
  `name_gr` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_gr` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `gr_product`
--

INSERT INTO `gr_product` (`id_gr_product`, `name_gr`, `date_gr`) VALUES
(1, 'Men', '2018-08-17 00:00:00'),
(2, 'Girls', '2018-08-17 02:00:00'),
(3, 'New', '2018-08-17 02:00:00'),
(4, 'Sale', '2018-08-17 03:00:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product`
--

CREATE TABLE `product` (
  `id_product` int(10) NOT NULL,
  `id_gr_product` int(10) DEFAULT NULL,
  `name_product` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price_name` int(50) DEFAULT NULL,
  `description_product` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `img_product` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_product` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `product`
--

INSERT INTO `product` (`id_product`, `id_gr_product`, `name_product`, `price_name`, `description_product`, `img_product`, `date_product`) VALUES
(1, 1, 'Hàng Men chuẩn hình', 2, 'hàng đẹp', 'acsses/images/home/product1.jpg', NULL),
(2, 2, 'hàng gái', 2, 'dẹp luôn', 'acsses/images/home/product2.jpg', '2018-08-17 02:00:00'),
(3, 3, 'Hàng new mới', 3, 'quá đẹp', 'acsses/images/home/product3.jpg', '2018-08-17 02:00:00'),
(4, 4, 'Hàng sale', 4, 'Hàng sale nên rẻ lắm', 'acsses/images/home/product4.jpg', '2018-08-17 02:00:00'),
(5, 1, 'mạc văn toàn', 5, 'quá đẹp', 'acsses/images/home/product5.jpg', '2018-08-17 02:00:00'),
(6, 2, 'mạc văn toafnm 4', 7, 'hàng con gái', 'acsses/images/home/product5.jpg', '2018-08-17 02:00:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(50) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`) VALUES
(30, 'tenmoi', '', 'e10adc3949ba59abbe56e057f20f883e'),
(31, 'macvantoanhihihi', 'toanmac113@gmail.com', 'b9c28d2811d9eb33ad442e2df52141dc'),
(32, 'macvantoan', 'toanmac113@gmail.com', 'e10adc3949ba59abbe56e057f20f883e'),
(33, 'moi', 'toanmac113@gmail.com', '202cb962ac59075b964b07152d234b70');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id_contact`);

--
-- Chỉ mục cho bảng `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `cus_order`
--
ALTER TABLE `cus_order`
  ADD PRIMARY KEY (`id_order_cus`),
  ADD KEY `id` (`id`);

--
-- Chỉ mục cho bảng `dentails_order`
--
ALTER TABLE `dentails_order`
  ADD PRIMARY KEY (`id_order_details`),
  ADD KEY `id_order_cus` (`id_order_cus`),
  ADD KEY `id_product` (`id_product`);

--
-- Chỉ mục cho bảng `gr_product`
--
ALTER TABLE `gr_product`
  ADD PRIMARY KEY (`id_gr_product`);

--
-- Chỉ mục cho bảng `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id_product`),
  ADD KEY `id_gr_product` (`id_gr_product`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `contact`
--
ALTER TABLE `contact`
  MODIFY `id_contact` int(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `cus_order`
--
ALTER TABLE `cus_order`
  MODIFY `id_order_cus` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `dentails_order`
--
ALTER TABLE `dentails_order`
  MODIFY `id_order_details` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `gr_product`
--
ALTER TABLE `gr_product`
  MODIFY `id_gr_product` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `product`
--
ALTER TABLE `product`
  MODIFY `id_product` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `cus_order`
--
ALTER TABLE `cus_order`
  ADD CONSTRAINT `cus_order_ibfk_1` FOREIGN KEY (`id`) REFERENCES `users` (`id`);

--
-- Các ràng buộc cho bảng `dentails_order`
--
ALTER TABLE `dentails_order`
  ADD CONSTRAINT `dentails_order_ibfk_1` FOREIGN KEY (`id_order_cus`) REFERENCES `cus_order` (`id_order_cus`),
  ADD CONSTRAINT `dentails_order_ibfk_2` FOREIGN KEY (`id_product`) REFERENCES `product` (`id_product`);

--
-- Các ràng buộc cho bảng `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`id_gr_product`) REFERENCES `gr_product` (`id_gr_product`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
